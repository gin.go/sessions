package sessions

import (
	"math/rand"
	"sync"
	"time"

	"gitlab.com/gin.go/gin"
)

// Session ...
type Session struct {
	name  string
	value map[string]interface{}
	timer *time.Timer
}

// DefaultSessionEraseTimeout ...
const DefaultSessionEraseTimeout = 10 * time.Minute

var sessionChain = make(map[string]*Session)
var lock = sync.RWMutex{}
var cookieName string
var expireTime time.Duration
var expireHandler func(session *Session)

// Default ...
func Default() gin.HandlerFunc {
	return New("token", DefaultSessionEraseTimeout, func(session *Session) {})
}

// New ...
func New(name string, expire time.Duration, handler func(session *Session)) gin.HandlerFunc {
	cookieName = name
	expireTime = expire
	expireHandler = handler
	return func(c *gin.Context) {
		var session *Session
		token, err := c.Cookie(cookieName)
		if err != nil {
			token = randomString(32) // random
			c.SetCookie(cookieName, token, 0, "/", "", false, true)
		}
		session = get(token)
		if session == nil {
			session = create(token)
		}
		c.Next()
		refresh(session)
	}
}

// create ...
func create(name string) *Session {
	session := new(Session)
	session.name = name
	session.value = make(map[string]interface{})
	handler := func() {
		timeout(session)
	}
	session.timer = time.AfterFunc(expireTime, handler)
	lock.Lock()
	sessionChain[name] = session
	lock.Unlock()
	return session
}

func get(name string) *Session {
	lock.RLock()
	s, ok := sessionChain[name]
	lock.RUnlock()
	if !ok {
		return nil
	}
	return s
}

// Get ...
func (s *Session) Get(key string) interface{} {
	v, ok := s.value[key]
	if !ok {
		return nil
	}
	return v
}

// Set ...
func (s *Session) Set(key string, value interface{}) {
	s.value[key] = value
}

// Delete ...
func (s *Session) Delete(key string) {
	delete(s.value, key)
}

// GetSession ...
func GetSession(c *gin.Context) *Session {
	token, err := c.Cookie(cookieName)
	if err != nil {
		return nil
	}
	return get(token)
}

func timeout(session *Session) {
	go func() {
		expireHandler(session)
	}()
	lock.Lock()
	delete(sessionChain, session.name)
	lock.Unlock()
}

func refresh(session *Session) {
	session.timer.Stop()
	session.timer.Reset(expireTime)
}

func randomString(number int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < number; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
